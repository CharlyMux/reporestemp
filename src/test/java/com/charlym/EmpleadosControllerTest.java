package com.charlym;


import com.charlym.utils.BadSeparator;
import com.charlym.utils.EstadosPedido;
import com.charlym.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;


import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@EnableConfigurationProperties
@SpringBootTest
public class EmpleadosControllerTest {
    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testGetCadena() {
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen = "luz del sol";
        assertEquals(correcto, empleadosController.getCadena(origen, "."));
    }

    @Test
    public void tesSeparadorGetCadena(){
        try{
            Utilidades.getCadena("Carlos Mujica", "..");
            fail("Se esperaba BadSeparator");
        }catch (BadSeparator bs) {}
    }

    @Test
    public void testGetAutor() {
        assertEquals("Carlos Mujica", empleadosController.getAppAutor());
    }

    @ParameterizedTest
    @ValueSource(ints ={1, 3, 5, -3, Integer.MAX_VALUE})
    public void testEsImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings ={"", " "})
    public void testEstaBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings ={" ", "\t", "\n"})
    public void testEstaBlancoCompleto(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadoPedido(EstadosPedido ep){
        assertTrue(Utilidades.valorarEstadoPedido(ep));
    }

}
